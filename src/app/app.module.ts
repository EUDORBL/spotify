import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//Rutas



//Servicio

//Componente
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { AsidebarComponent } from './components/shared/asidebar/asidebar.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import {HttpClientModule} from '@angular/common/http';
import { RedesSocialesComponent } from './components/common/redes-sociales/redes-sociales.component';
import { HomeComponent } from './components/home/home.component';
import { SearchComponent } from './components/search/search.component';
import { ArtistaComponent } from './components/artista/artista.component';
import { ContainerComponent } from './components/shared/container/container.component';
import { NoimgPipe } from './pipes/noimg.pipe';
import { LoadingComponent } from './components/shared/loading/loading.component';
import { HttpseguroPipe } from './pipes/httpseguro.pipe';
import { RegistroComponent } from './components/registro/registro.component';
import { LoginComponent } from './components/login/login.component';
import { InicioComponent } from './components/inicio/inicio.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    AsidebarComponent,
    FooterComponent,
    RedesSocialesComponent,
    HomeComponent,
    SearchComponent,
    ArtistaComponent,
    ContainerComponent,
    NoimgPipe,
    LoadingComponent,
    HttpseguroPipe,
    RegistroComponent,
    LoginComponent,
    InicioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
