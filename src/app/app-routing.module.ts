import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ArtistaComponent } from './components/artista/artista.component';
import { LoginComponent } from './components/login/login.component';
import { RegistroComponent } from './components/registro/registro.component';
import { AuthGuard } from './guards/auth.guard';
import { InicioComponent } from './components/inicio/inicio.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent},
  { path: 'registro', component: RegistroComponent},
  { path: 'inicio', component: InicioComponent,
  canActivate:[AuthGuard],
  children:[
    { path: 'home/:inputBuscar', component: HomeComponent, canActivate:[AuthGuard]},
    { path: 'artista/:id', component: ArtistaComponent, canActivate:[AuthGuard]},
    { path: '', component: HomeComponent},
    { path: '**', component: HomeComponent}
  ]

  },
  { path: '', component: LoginComponent},
    { path: '**', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
