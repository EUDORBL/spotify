import { Component, OnInit } from '@angular/core';
import { UserModule } from '../models/user/user.module';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styles: [
  ]
})
export class RegistroComponent implements OnInit {
  usuario:UserModule;
  constructor(private authService:AuthService) { }

  ngOnInit(): void {
    this.usuario = new UserModule();
    this.usuario.email;
  }


  onSubmitRegistro(){
    console.log("listo para escuchar");
    console.log(this.usuario);
    this.authService.nRegistroUsers(this.usuario)
                    .subscribe(
                      result => {console.log(result)},
                      (errorReturn:any)=>{
                        console.log(errorReturn);
                      }
                    );
  }

}
