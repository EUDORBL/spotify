import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators'
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  spotifyCredenciales:string;

  constructor( private http:HttpClient,
                private authService:AuthService) { }



  getUrlSend(query:string){
    this.spotifyCredenciales = this.authService.getToken();
    if(this.spotifyCredenciales !== null){
     const headers = new HttpHeaders({
      'Authorization' : `Bearer ${JSON.parse(this.spotifyCredenciales)['access_token']}`
    });

    const uri = `https://api.spotify.com/v1/${query}`;

    return this.http.get(uri,{headers});
    }


  }

  getNewReleases(){
    return this.getUrlSend("browse/new-releases")
                .pipe(map(data => data['albums'].items));
    }

  getBuscarArtista(inputBuscar:string){
    return this.getUrlSend(`search?q=${inputBuscar}&type=artist&limit=20`)
    .pipe(map(data => data['artists'].items));
  }

  getDetallesArtista(id:string){
    return this.getUrlSend(`artists/${id}`)
    .pipe(map(data => data));
  }

  getCancionesArtista(id:string){
    return this.getUrlSend(`artists/${id}/top-tracks?country=ES`)
    .pipe(map(data => data['tracks']));
  }

}
