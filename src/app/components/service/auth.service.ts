import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { UserModule } from '../models/user/user.module';
import { map } from 'rxjs/operators';
import { pipe } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public spotifyIdentity:string;
  private user:UserModule;
  constructor(private http:HttpClient,private router:Router) {
      this.user;
      this.getToken();
   }

   private url:string ="relyingparty";
   private apiKey:string ="AIzaSyCgqLtDgWQsn4Kot_OILHiQDFgYCVf2DZU";


   offLogout(){
    localStorage.removeItem('accessSpotify');
    return this.router.navigate([`/login`]);
  }

  onLogin(usuario:UserModule){
    usuario.returnSecureToken=true;
    return this.http.post<UserModule>(`${this.url}/verifyPassword?key=${this.apiKey}`,usuario)
                .pipe(map(resul =>{
               this.postAutentificacion(resul).subscribe();
               return resul;
                  }));
  }





  nRegistroUsers(usuario:UserModule){
    usuario.returnSecureToken=true;
    return this.http.post(`${this.url}/signupNewUser?key=${this.apiKey}`,usuario);
  }



  postAutentificacion(user:UserModule){
      const body = new HttpParams()
    .set('grant_type', 'client_credentials')
    .set('client_id', 'ec98c4551e6c482aa7157d1de85d623b')
    .set('client_secret', 'c3e79225118f4397a8fd1837b88a11b9');

    return this.http.post<UserModule>(`api/token`,body.toString(),
    {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    }).pipe(map(result =>{
      user.access_token = result.access_token;
      user.expires_in = result.expires_in;
      user.scope = result.scope;
      user.token_type = result.token_type;
      return this.setToken(JSON.stringify(user))
    }));
  }

  setToken(accessSpotify:string){
      this.spotifyIdentity = accessSpotify;
      localStorage.setItem('accessSpotify',this.spotifyIdentity);
  }

  getToken = () => {
    if(localStorage.getItem('accessSpotify')){
      this.spotifyIdentity=localStorage.getItem('accessSpotify');
    }else{
      this.spotifyIdentity=null;
    }
    return this.spotifyIdentity;
  }

  isAutenticado(){
    return !(this.spotifyIdentity);
  }


}
