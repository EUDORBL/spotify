import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'app-asidebar',
  templateUrl: './asidebar.component.html',
  })
export class AsidebarComponent implements OnInit {
  public user:any;
  public email:string;
  constructor(private authService: AuthService) {
    this.user = JSON.parse(this.authService.getToken());

   }

  ngOnInit(): void {
    this.email=this.user?this.user.email:null;
  }

}
