import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../service/auth.service';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  })
export class NavbarComponent implements OnInit {
  public isActivo:boolean;
  constructor(private router:Router, private authServer:AuthService) {

   }

   ngOnInit(): void {
    this.isActivo = !(this.authServer.isAutenticado());
   }
  inputAccionBuscar(inputBuscar){
     this.router.navigate([`inicio/home/${inputBuscar}`]);
  }

  logout(){
    this.authServer.offLogout();

  }

}
