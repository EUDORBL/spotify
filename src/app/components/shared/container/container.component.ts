import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styles: [
    '.content-wrapper {margin-left:150px !important}'
  ]
})
export class ContainerComponent implements OnInit {
  constructor() {

  }

  ngOnInit(): void {
  }

}
