import { Component, OnInit,Output } from '@angular/core';
import { SpotifyService } from '../service/spotify.service';
import { ActivatedRoute, Router} from '@angular/router';
import { AuthService } from '../service/auth.service';
import { UserModule } from '../models/user/user.module';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: [
  ]
})
export class HomeComponent{

  canciones: any[];
  loading:boolean;
  public artistas:any[];
 constructor( private spotify:SpotifyService,
              private rutaActiva: ActivatedRoute,
              private authService: AuthService,
              private router:Router) {

     if(this.authService.isAutenticado()){
       this.router.navigate([`/login`]);
     }


  this.loading = true;
  this.rutaActiva.params.subscribe(params =>{
    if(!params.inputBuscar){
    this.spotify.getNewReleases().subscribe((data:any) => {
      this.canciones = data;
      this.loading=false;
      console.log(data);
    });
    }else{
      this.buscar(params.inputBuscar);
    }
  });


   }


  buscar(inputBuscar:string){
    this.spotify.getBuscarArtista(inputBuscar)
          .subscribe((artista:any) => {
          this.artistas = artista;
          this.loading=false;
          });
  }




}
