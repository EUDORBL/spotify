import { Component, OnInit } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { SpotifyService } from '../service/spotify.service';

@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styles: [
    '.widget-user-image {left:44% !important;z-index:100 !important;top:50px}',
    '.img-circle {width:220px !important}',
    '.attachment-img {max-height:50px;max-width:50px;}',
    '.progress {height:16px !important}'
  ]
})
export class ArtistaComponent implements OnInit {
  public loading:boolean;
  public canciones:any[];
  public artista:any[];
  constructor(private activatedRoute:ActivatedRoute, private spotify:SpotifyService) {
    this.loading=true;
    this.activatedRoute.params.subscribe(params =>{
        if(params.id){
          this.spotify.getDetallesArtista(params.id)
                      .subscribe((artista:any) => {
                        console.log(artista)
                        this.artista=artista;
                        if(artista){
                          this.spotify.getCancionesArtista(params.id).subscribe(canciones =>{
                            this.canciones=canciones;
                            console.log(canciones);
                            this.loading=false;
                          });
                        }
                      });
        }

      });
   }

  ngOnInit(): void {

  }

}
