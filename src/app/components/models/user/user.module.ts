import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class UserModule {
  email:string;
  nombre:string;
  password:string;
  access_token:string;
  token_type: string;
  expires_in: number;
  scope: number;
  dateIni:Date;
  idToken:string;
  localId:string;
  refreshToken:string;
  returnSecureToken:boolean;

    constructor(){
     this.dateIni = new Date();
    }


 }
