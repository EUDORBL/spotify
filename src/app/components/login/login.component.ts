import { Component, OnInit } from '@angular/core';
import { UserModule } from '../models/user/user.module';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {
  usuario:UserModule;
  constructor(private authService:AuthService,private router:Router) {
    this.usuario;
   }

  ngOnInit(): void {
    this.usuario = new UserModule();
    this.usuario.email;

  }
  onSubmitLogin(){
    console.log("listo para escuchar");
    console.log(this.usuario);
    this.authService.onLogin(this.usuario)
                    .subscribe(
                      result => {
                        console.log(result)
                        this.router.navigate([`/inicio/`]);
                      },(errorReturn:any)=>{
                        console.log(errorReturn);
                      }
                    );

  }

}
